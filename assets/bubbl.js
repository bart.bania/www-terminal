function Shell() {
    var notimplemented = 'To be implemented soon. (or not)\n';
    this.init = function () {
        $(window).load(function() {
            // Creating the console.
            var header = "Welcome to www.bartbania.com - The Unwritten Words.\n" +
                     "    Type '\033[1mhelp\033[0m' for a list of available commands.\n";
            window.jqconsole = $('body').jqconsole(header, '$ -> ');

            // Abort prompt on Ctrl+Z.
            jqconsole.RegisterShortcut('Z', function() {
            jqconsole.AbortPrompt();
            handler();
            });

            // Move to line start Ctrl+A.
            jqconsole.RegisterShortcut('A', function() {
            jqconsole.MoveToStart();
            handler();
            });

            // Move to line end Ctrl+E.
            jqconsole.RegisterShortcut('E', function() {
            jqconsole.MoveToEnd();
                handler();
            });

            jqconsole.RegisterMatching('{', '}', 'brace');
            jqconsole.RegisterMatching('(', ')', 'paran');
            jqconsole.RegisterMatching('[', ']', 'bracket');
            // Handle a command.
            var handler = function(command) {
            if (command) {
                try {
                  Shell[command]();
                } catch (e) {
                  jqconsole.Write(command + ': command not found\n');
                }
            }
            jqconsole.Prompt(true, handler, function(command) {
                // Continue line if can't compile the command.
                try {
                Function(command);
                } catch (e) {
                if (/[\[\{\(]$/.test(command)) {
                    return 1;
                } else {
                    return 0;
                }
                }
                return false;
            });
            };

            // Initiate the first prompt.
            handler();
        });
    }
    this.help = function () {
        var help =
        '\033[1mList of available commands:\033[0m\n' +
        '\033[33mblog\033[0m ==> \033[36mBart\'s blog.\n' +
        '\033[33mcontact\033[0m ==> \033[36mContact info.\n' +
        '\033[33mdate\033[0m ==> \033[36mDisplays the current date.\n' +
        '\033[33mhelp\033[0m ==> \033[36mhelp Displays this list.\n' +
        '\033[33mresume\033[0m ==> \033[36mDisplays a compact resume.\n' +
        '\033[33mskills\033[0m ==> \033[36mProfessional skills.\n' +
        '\033[33mwhois\033[0m ==> \033[36mWho is Bart Bania?\033[0m\n' +
        '\033[33mping\033[0m ==> \033[36m \033[0m\n' +
        '\nThere\'s some other available commands. Use your imagination :-)\n';
        jqconsole.Write(help);
    }
    this.blog = function () {
        window.location="http://www.bartbania.com/";
    }
    this.contact = function () {
        window.location.href = "mailto:contact.unwords@gmail.com";
    }
    this.date = function () {
        var date = 'In a relationship with \033[1mEwelina\033[0m\r\n';
        jqconsole.Write(date);
    }
    this.resume = function () {
        jqconsole.Write(notimplemented);
    }
    this.skills = function () {
        jqconsole.Write(notimplemented);
    }
    this.whois = function () {
        var whois = ('\033[1;33mBart Bania\033[0m   is a bright Linux lover and an IT technician,\r\n             beginner programmer focused on Python and JS.\r\n\n             "I am a poet and technophile, selective geek.\r\n             Confident introvert. Quiet, but not shy.             \r\n             I love learning and am an omnivorous reader."\n');
        jqconsole.Write(whois);
    }
    this.ping = function () {
        var ping = '\033[1;31mPONG!\033[0m\r\n';
        jqconsole.Write(ping);
    }

};
var Shell = new Shell();
Shell.init();
